module Main where

import Utils.Messaging
import Utils.Monads

import Control.Concurrent (forkIO, killThread, threadDelay)
import Data.List (intercalate)
import Data.Time.Clock (UTCTime, diffUTCTime, getCurrentTime)
import GHC.IO.Handle (Handle)
import System.Environment (getArgs)
import System.Exit (ExitCode(ExitSuccess))
import System.IO (hGetLine, hPutStr, hPutStrLn, hIsEOF, stdout, stderr)
import System.Process (StdStream(CreatePipe), createProcess, proc, readProcessWithExitCode, waitForProcess)

-- TODO: consider the possibility of making and handling a command queue to make other commands run when/if the current command completes successfully

rightPadAlign :: Int -> String -> String
rightPadAlign alignment string = string ++ (take (alignment - (length string `mod` alignment)) $ repeat ' ')

forEachLineIn :: Handle -> (String -> IO ()) -> IO ()
forEachLineIn handle function = do
	hitEOF <- hIsEOF handle
	if hitEOF then
		return ()
	else do
		line <- hGetLine handle
		function line
		forEachLineIn handle function

everySecond :: IO () -> IO ()
everySecond function = do
	function
	threadDelay 1000
	everySecond function

showTime :: Int -> String
showTime seconds = do
	let timeNumbers = dropWhile (==0) [seconds `div` 86400, seconds `div` 3600, seconds `div` 60, seconds `mod` 60]
	if null timeNumbers then
		"0"
	else do
		let formatNumberString = \numberString -> drop (length numberString - 2) $ '0':numberString
		intercalate ":" $ map (formatNumberString . show) timeNumbers

getStatus :: UTCTime -> [String] -> IO String
getStatus startTime runningCommand = do
	time <- getCurrentTime
	let elapsedTimeString = showTime $ round $ diffUTCTime time startTime

	(terminalWidthExitCode, terminalWidthStringOrEmpty, _) <- readProcessWithExitCode "tput" ["cols"] ""
	let terminalWidth = if terminalWidthExitCode == ExitSuccess then (read terminalWidthStringOrEmpty :: Int) else 80

	let elapsedTimeWithSpacing = rightPadAlign 8 elapsedTimeString
	let displayRunningCommand = take (terminalWidth - length elapsedTimeWithSpacing) $ intercalate " " runningCommand
	return (elapsedTimeWithSpacing ++ "\ESC[1m" ++ displayRunningCommand ++ "\ESC[0m")

updateStatus :: UTCTime -> [String] -> Handle -> String -> IO ()
updateStatus startTime runningCommand output line = do
	hPutStrLn output ("\r\ESC[2K" ++ line)
	status <- getStatus startTime runningCommand
	hPutStr output status

main = doFallibleMain $ tryIO $ do
	commandToRun <- getArgs
	if null commandToRun then
		err "No command to run was given!"
	else do
		startTime <- getCurrentTime
		(_, Just commandStdout, Just commandStderr, commandProcess) <- createProcess (proc (head commandToRun) (tail commandToRun)){ std_out = CreatePipe, std_err = CreatePipe }

		stdoutThread <- forkIO $ forEachLineIn commandStdout $ updateStatus startTime commandToRun stdout
		stderrThread <- forkIO $ forEachLineIn commandStderr $ updateStatus startTime commandToRun stderr
		timedThread <- forkIO $ everySecond $ updateStatus startTime commandToRun stdout ""

		waitForProcess commandProcess

		-- TODO

		killThread timedThread
		killThread stdoutThread
		killThread stderrThread
